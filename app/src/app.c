/*============================================================================
 * Autor: SIMM
 * Licencia: 
 * Fecha: 06/06/20
 *===========================================================================*/
// DEPENDENCIAS

#include "app.h"                        // <= Su propia cabecera (opcional)
#include "sapi.h"                       // <= Biblioteca sAPI
#include "driverTecladoMatricial.h"     // <= Teclado matricial
#include "driverDisplay7Seg.h"	      // <= Display matricial

// VARIABLES
typedef enum{PRIMER_NUM, SEGUNDO_NUM, RESULTADO} estados;

//variable de estado global
estados estadoActual;
uint16_t numeroTemporal=0;
uint16_t primerNumero = 0;
uint16_t segundoNumero = 0;
uint16_t resultado = 0;

char operacion;
char result[4] = {};

// FUNCIONES
// FUNCION PRINCIPAL, PUNTO DE ENTRADA AL PROGRAMA LUEGO DE ENCENDIDO O RESET.
void InicializarMEF();
void ActualizarMEF(uint16_t valorIngresado);
uint16_t RealizarOperacion(uint16_t primerNum, uint16_t segundoNum, char operacion);

void printLineUART(const char* str1, const char* str2) {
    uartWriteString( UART_USB,  str1);
    uartWriteString( UART_USB,  ": ");
    uartWriteString( UART_USB,  str2);
    uartWriteString( UART_USB, "\r\n");
}

// FUNCION PRINCIPAL, PUNTO DE ENTRADA AL PROGRAMA LUEGO DE ENCENDIDO O RESET.
int main( void )
{
   // ---------- CONFIGURACIONES ------------------------------

   InicializarMEF();

   // ---------- REPETIR POR SIEMPRE --------------------------
   while( TRUE ) {
      if(leerTecladoMatricial()){
         uint16_t valorIngresado = keypadKeys[key];
         printLineUART("key", intToString(valorIngresado));
         ActualizarMEF(valorIngresado);
      }
   }
   // NO DEBE LLEGAR NUNCA AQUI, debido a que a este programa se ejecuta
   // directamenteno sobre un microcontroladore y no es llamado por ningun
   // Sistema Operativo, como en el caso de un programa para PC.
   return 0;
}

uint16_t RealizarOperacion(uint16_t primerNum, uint16_t segundoNum, char operacion) {
   switch(operacion) {
   case '+':
   return primerNum + segundoNum;
   case '-':
   return primerNum - segundoNum;
   case '*':
   return primerNum * segundoNum;
   case '/':
   return primerNum / segundoNum;
   }
}

void InicializarMEF() {
   boardConfig();                         // Inicializar y configurar la plataforma
   
   display7SegmentosConfigurarPines(); 	// Configuraci?n de pines para el display 7 segmentos
   configurarTecladoMatricial(); 		   // Configurar teclado   

   estadoActual = PRIMER_NUM;             // Setea estado inicial
}

void ActualizarMEF(uint16_t valorIngresado) {
   switch(estadoActual) {
         case PRIMER_NUM:
            if(valorIngresado < 10) {
               numeroTemporal = numeroTemporal * 10 + valorIngresado;
            }
            if(valorIngresado >= 10 && valorIngresado<=13) {
               primerNumero = numeroTemporal;
               estadoActual = SEGUNDO_NUM;
               operacion = asciiKeypadKeys[key];
               if (asciiKeypadKeys[key] == '-') {
                  // LOGICA PARA NUMEROS NEGATIVOS
               }
            }
            if(valorIngresado == 14) {
               estadoActual = PRIMER_NUM;
               numeroTemporal = 0;
            }
            printLineUART("primerNumero", intToString(primerNumero));
            itoaDisplay(primerNumero, result, 10);
            display7SegmentosEscribir4Digitos(result);        
         break;
         case SEGUNDO_NUM:
            if(valorIngresado < 10) {
               numeroTemporal = numeroTemporal * 10 + valorIngresado;
            }
            if((valorIngresado >= 10 && valorIngresado <= 13) || valorIngresado == 15) {
               estadoActual = RESULTADO;
               // CUALQUIERA QUE TOQUE VA A RESULTADO ?? 
            }
            if(valorIngresado == 14) {
               estadoActual = PRIMER_NUM;
               numeroTemporal = 0;
            }
            printLineUART("segundoNumero", intToString(segundoNumero));
            itoaDisplay(segundoNumero, result, 10);
            display7SegmentosEscribir4Digitos(result);   
         break;
         case RESULTADO:
            resultado = RealizarOperacion(primerNumero, segundoNumero, operacion);
            printLineUART("resultado", intToString(resultado));
            itoaDisplay(resultado, result, 10);
            display7SegmentosEscribir4Digitos(result);
         break;      
      }
}