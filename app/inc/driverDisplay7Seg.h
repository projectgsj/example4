/*============================================================================
 * Licencia:
 * Autor:
 * Fecha:
 *===========================================================================*/

#ifndef _DRIVERDISPLAY7SEG_H_
#define _DRIVERDISPLAY7SEG_H_

/*==================[inclusiones]============================================*/

/*==================[c++]====================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*==================[macros]=================================================*/
#define ANODO_COMUN 1
#define CHARS 26

#include "sapi.h"       	   // <= Biblioteca sAPI
/*==================[tipos de datos declarados por el usuario]===============*/

/*==================[declaraciones de datos externos]========================*/

uint8_t charsVec[CHARS];
uint8_t display7SegmentosPines[8];
uint8_t display7SegmentosPinesDigitos[4];
uint8_t display7SegmentosSalidas[CHARS];

/*==================[declaraciones de funciones externas]====================*/

void display7SegmentosEscribir( uint8_t symbolIndex );
uint8_t charToIndex( uint8_t charIndex );
void display7SegmentosEscribirDigito( uint8_t digitIndex );
char* itoaDisplay(int value, char* result, int base);
void display7SegmentosConfigurarPines( void );
void display7SegmentosEscribir4Digitos( uint8_t* digit );

/*==================[c++]====================================================*/
#ifdef __cplusplus
}
#endif
#endif