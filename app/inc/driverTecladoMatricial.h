/*============================================================================
 * Licencia:
 * Autor:
 * Fecha:
 *===========================================================================*/

#ifndef _DRIVERTECLADOMATRICIAL_H_
#define _DRIVERTECLADOMATRICIAL_H_


/*==================[inclusiones]============================================*/

/*==================[c++]====================================================*/

#ifdef __cplusplus
extern "C" {
#endif

/*==================[macros]=================================================*/

#include "sapi.h"       	   // <= Biblioteca sAPI

/*==================[tipos de datos declarados por el usuario]===============*/

/*==================[declaraciones de datos externos]========================*/

uint16_t key;

uint8_t keypadRowPins[4];
uint8_t keypadColPins[4];
uint16_t asciiKeypadKeys[16];
uint16_t keypadKeys[16];

/*==================[declaraciones de funciones externas]====================*/

void configurarTecladoMatricial( void );
bool_t leerTecladoMatricial( void );

/*==================[c++]====================================================*/
#ifdef __cplusplus
}
#endif
#endif